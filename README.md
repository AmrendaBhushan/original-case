Travel API Client 
=================
To view the solution (after starting the application) please go to:

http://localhost:9000/travel/homepage.xhtml

The API Server name and port is defined under application.yml file 

ws:
  uri: http://localhost:8080
  
Please change the API Server uri configuration if different  