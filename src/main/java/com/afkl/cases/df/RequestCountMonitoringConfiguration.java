package com.afkl.cases.df;

import static org.springframework.http.HttpStatus.OK;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

import lombok.extern.slf4j.Slf4j;;
@Slf4j
@Configuration
class RequestCountMonitoringConfiguration extends WebMvcConfigurerAdapter {
	private Meter totalNoOfRequestMeter;
	private Meter totalNoOfOkRequestMeter;
	private Meter totalNoOf4xxRequestMeter;
	private Meter totalNoOf5xxRequestMeter;
	private Timer responseTimer;
	private Timer.Context timer ;

	@Autowired
	public RequestCountMonitoringConfiguration(MetricRegistry metricRegistry) {
		this.totalNoOfRequestMeter = metricRegistry.meter("totalNoOfRequestProcessed");
		this.totalNoOfOkRequestMeter = metricRegistry.meter("totalNoOf200Request");
		this.totalNoOf4xxRequestMeter=metricRegistry.meter("totalNoOf4xxRequestMeter");
		this.totalNoOf5xxRequestMeter=metricRegistry.meter("totalNoOf5xxRequestMeter");
		this.responseTimer=metricRegistry.timer("responseTimer");
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new HandlerInterceptorAdapter() {
			@Override
			public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
					Exception ex) throws Exception {
				totalNoOfRequestMeter.mark();
				timer.stop();
				String responseStatus=Integer.toString(response.getStatus());
				if (responseStatus.equals( String.valueOf(OK.value()))){
					totalNoOfOkRequestMeter.mark();
				}else if(Pattern.matches("^4\\d{2}$",responseStatus)){
					totalNoOf4xxRequestMeter.mark();
				}else if(Pattern.matches("^5\\d{2}$",responseStatus)){
					totalNoOf5xxRequestMeter.mark();
				} else{
					 log.info(String.format("Other Type of Response Code and the Code is [%d]", responseStatus));
				}
			}
			
			@Override
			public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
				throws Exception {
				timer=responseTimer.time();
				return true;
			}
			
		});
	}
}
