package com.afkl.cases.df.client;

public interface TravelClient {
	    String getAirportDetails(String size,String page,String term);
	    String getFare(String origin, String destination);
}
