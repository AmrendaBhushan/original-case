package com.afkl.cases.df.client;

import java.net.URI;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.afkl.cases.df.common.TravelConstant;
import com.afkl.cases.df.pojo.AccessToken;

@RestController
public class TravelClientImpl implements TravelClient {

	@Value("${ws.airport.uri}")
	private URI airporturi;

	@Value("${ws.fare.uri}")
	private String fareuri;

	@Value("${ws.authorization}")
	private String authorization;

	@Value("${security.oauth2.client.grant-type}")
	private String grantType;

	@Value("${security.oauth2.client.id}")
	private String clientId;

	@Value("${security.oauth2.client.client-secret}")
	private String clientSecret;

	@Value("${security.oauth2.client.access-token-uri}")
	private URI accessTokenURI;

	private AccessToken accessToken;
	private long tokenRefreshedRequiredAt;

	@Bean
	protected RestTemplateBuilder builder() {
		RestTemplateBuilder builder = new RestTemplateBuilder();
		return builder;
	}

	@Bean
	protected HttpHeaders httpHeaders() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		httpHeaders.set(HttpHeaders.AUTHORIZATION, authorization);
		return httpHeaders;
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		builder.setConnectTimeout(5000);
		return builder.build();
	}

	@Override
	@RequestMapping("/airportSearch")
	public String getAirportDetails(@QueryParam("size") String size, @QueryParam("page") String page,
			@QueryParam("term") String term) {
		UriComponentsBuilder builder;
		builder = UriComponentsBuilder.fromUri(airporturi);
		if (!StringUtils.isEmpty(size) && !StringUtils.isEmpty(page)) {
			builder = builder.queryParam("size", size);
			builder = builder.queryParam("page", page);
		} else if (!StringUtils.isEmpty(term)) {
			builder = builder.queryParam("term", term);
		} else {
			throw new IllegalArgumentException();
		}

		builder = builder.queryParam(TravelConstant.QUERY_ACCESS_TOKEN, this.getToken().getAccess_token());
		URI finalURI = builder.build().encode().toUri();
		HttpEntity<String> httpEntity = new HttpEntity<String>(new HttpHeaders());
		ResponseEntity<String> response = restTemplate(builder()).exchange(finalURI, HttpMethod.GET, httpEntity,
				String.class);
		return handleResponse(response);
	}

	@Override
	@RequestMapping("/queryFare/{origin}/{destination}")
	public String getFare(@PathVariable("origin") String origin, @PathVariable("destination") String destination) {
		if (TravelConstant.UNDEFINED.equalsIgnoreCase(origin) || StringUtils.isEmpty(origin)
				|| TravelConstant.UNDEFINED.equalsIgnoreCase(destination) || StringUtils.isEmpty(destination)) {
			throw new IllegalArgumentException();
		}
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(String.format(fareuri, origin, destination))
				.queryParam(TravelConstant.QUERY_ACCESS_TOKEN, this.getToken().getAccess_token());
		URI finalURI = builder.build().toUri();
		HttpEntity<String> httpEntity = new HttpEntity<String>(new HttpHeaders());
		ResponseEntity<String> response = restTemplate(builder()).exchange(finalURI, HttpMethod.GET, httpEntity,
				String.class);
		return handleResponse(response);
	}

	private boolean isTokenAlive() {
		boolean result = true;
		if (System.currentTimeMillis() - tokenRefreshedRequiredAt > 1000) {
			result = false;
		}
		return result;

	}

	private AccessToken getToken() {

		if (isTokenAlive()) {
			return this.accessToken;
		} else {
			AccessToken accessToken = getNewToken();
			tokenRefreshedRequiredAt = System.currentTimeMillis() + Long.valueOf(accessToken.getExpires_in()) * 1000;
			this.accessToken = accessToken;
			return accessToken;
		}
	}

	private AccessToken getNewToken() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUri(accessTokenURI)
				.queryParam(TravelConstant.GRANT_TYPE, grantType).queryParam(TravelConstant.CLIENT_ID, clientId)
				.queryParam(TravelConstant.CLIENT_SECRET, clientSecret);
		URI finalURI = builder.build().encode().toUri();
		HttpEntity<String> httpEntity = new HttpEntity<String>(httpHeaders());
		ResponseEntity<AccessToken> response = restTemplate(builder()).exchange(finalURI, HttpMethod.POST, httpEntity,
				AccessToken.class);
		return response.getBody();
	}

	private String handleResponse(ResponseEntity<String> response) {
		String responseString = null;
		HttpStatus statusCode = response.getStatusCode();
		if (statusCode == HttpStatus.OK) {
			responseString = response.getBody();
		} else if (statusCode.is4xxClientError()) {
			throw new IllegalArgumentException();
		} else if (statusCode.is5xxServerError()) {
			throw new InternalServerErrorException();
		}
		return responseString;
	}

}
