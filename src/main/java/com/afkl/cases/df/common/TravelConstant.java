package com.afkl.cases.df.common;

public class TravelConstant {

	public static final String QUERY_Term = "term";
	public static final String QUERY_ACCESS_TOKEN = "access_token";
	public static final String GRANT_TYPE="grant_type";
	public static final String CLIENT_ID="client_id";
	public static final String CLIENT_SECRET="client_secret";
	public static final String UNDEFINED="undefined"; 
}
