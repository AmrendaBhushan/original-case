		var app=angular.module("autoCompleteApp",['zingchart-angularjs']);
		app.controller('autoCompleteCtrl',function($scope,$http,$location,$timeout){
			$scope.autoCompleteAirportOrg=function(searchString){
				var output=[];
				if(searchString && searchString.length>2){
					$http({
				        method : "GET",
				        url : "/travel/airportSearch?term="+searchString	  
				    }).then(function mySuccess(response) {
						$scope.airportJSONList = response.data._embedded;
						var breakLoop = false;
						angular.forEach($scope.airportJSONList.locations,function(airportsObj){
							if(!breakLoop){
								var airportCode = airportsObj.code;
								var airportName = airportsObj.name;
								var countryName = airportsObj.parent.parent.name;
								var description = airportsObj.description;
								
								if(airportCode.toLowerCase().indexOf(searchString.toLowerCase())>=0 
										|| airportName.toLowerCase().indexOf(searchString.toLowerCase())>=0
										||	countryName.toLowerCase().indexOf(searchString.toLowerCase())>=0
										|| description.toLowerCase().indexOf(searchString.toLowerCase())>=0){
									output.push(airportsObj);
									if(output.length==$scope.listingSize){
										breakLoop = true;
									}
								}
							}
						});
						$scope.filterAirportOrg=output;
				    }, function myError(response) {
				        $scope.apiResponse = response.statusText;
				        $scope.filterAirportOrg= null;
				    });
				}
			}
			
			
			$scope.autoCompleteAirportDest=function(searchString){
				var output=[];
				if(searchString.length>2){
					$http({
				        method : "GET",
				        url : "/travel/airportSearch?term="+searchString
				    }).then(function mySuccess(response) {
						$scope.airportJSONList = response.data._embedded;
						var breakLoop = false;
						angular.forEach($scope.airportJSONList.locations,function(airportsObj){
							if(!breakLoop){
								var airportCode = airportsObj.code;
								var airportName = airportsObj.name;
								var countryName = airportsObj.parent.parent.name;
								var description = airportsObj.description;
								
								if(airportCode.toLowerCase().indexOf(searchString.toLowerCase())>=0 
										|| airportName.toLowerCase().indexOf(searchString.toLowerCase())>=0
										||	countryName.toLowerCase().indexOf(searchString.toLowerCase())>=0
										|| description.toLowerCase().indexOf(searchString.toLowerCase())>=0){
									output.push(airportsObj);
									if(output.length==$scope.listingSize){
										breakLoop = true;
									}
								}
							}
						});
						$scope.filterAirportDest=output;
				    }, function myError(response) {
				        $scope.apiResponse = response.statusText;
				        $scope.filterAirportDest= null;
				    });
				}
			}
			
			$scope.fetchListOfAirports=function(searchString){
				var urlString = "/travel/airportSearch?size="+$scope.tableSizeListSelector+"&page="+$scope.currentPageNumber;
				if(searchString && null!=searchString){
					urlString = urlString +"&term="+searchString;
				}
				
			    $http({
			        method : "GET",
			        url : urlString
			    }).then(function mySuccess(response) {
			    	$scope.airportJSONListWhole = response.data;
			    	$scope.airportJSONList = $scope.airportJSONListWhole._embedded;	
					$scope.goToPageList=[];
					$scope.tableSizeList=[5,10,20];
					$scope.orderByIdentifier = 'name';
					
					$scope.airportJSONPageInfo = $scope.airportJSONListWhole.page;
					for(i=1;i<=$scope.airportJSONPageInfo.totalPages;i++){
						$scope.goToPageList.push(i);
					}
			    }, function myError(response) {
			    	$scope.airportJSONList = null;
			    });
			}
					
			$scope.fillOriginTextbox=function(stringObj){
				$scope.origin="("+stringObj.code+") "+stringObj.name+" , "+stringObj.parent.parent.name;
				$scope.originSelected = stringObj.code
				$scope.filterAirportOrg=null;
			}
			
			$scope.fillDestinationTextbox=function(stringObj){
				$scope.destination="("+stringObj.code+") "+stringObj.name+" , "+stringObj.parent.parent.name;
				$scope.destinationSelected = stringObj.code
				$scope.filterAirportDest=null;
			}
			
			$scope.initializePage=function(string){
				$scope.listingSize = 10;
				$scope.currentPageNumber = 1;
				$scope.tableSizeListSelector = 10;
				document.getElementById('firstPageButton').disabled = true;	
				document.getElementById('previousPageButton').disabled = true;
				
			}
			
			$scope.navigateToNextPage=function(){
				$scope.navigateToPage($scope.currentPageNumber+1,$scope.airportJSONPageInfo.totalPages);
			}
			
			$scope.navigateToPreviousPage=function(){
				$scope.navigateToPage($scope.currentPageNumber-1,$scope.airportJSONPageInfo.totalPages);
			}
			
			$scope.navigateToFirstPage=function(){
				$scope.navigateToPage(1,$scope.airportJSONPageInfo.totalPages);
			}
			
			$scope.navigateToLastPage=function(){
				$scope.navigateToPage($scope.airportJSONPageInfo.totalPages,$scope.airportJSONPageInfo.totalPages);
			}
			
			$scope.jumpToPage=function(){
				$scope.navigateToPage($scope.currentPageNumber,$scope.airportJSONPageInfo.totalPages);
			}
			
			$scope.changePageSize=function(){
				if(($scope.tableSizeListSelector * $scope.airportJSONPageInfo.totalPages) > $scope.airportJSONPageInfo.totalElements){					
					$scope.airportJSONPageInfo.totalPages = Math.ceil( $scope.airportJSONPageInfo.totalElements /  $scope.tableSizeListSelector);
					$scope.currentPageNumber = $scope.airportJSONPageInfo.totalPages;
					for(i=1;i<=$scope.airportJSONPageInfo.totalPages;i++){
						$scope.goToPageList.push(i);
					}
				}
				$scope.jumpToPage();
			}
			$scope.navigateToPage=function(pageNumber, totalSize){
				if(pageNumber < $scope.currentPageNumber){
					if(pageNumber>1){
						document.getElementById('firstPageButton').disabled = false;
						document.getElementById('previousPageButton').disabled = false;
						document.getElementById('lastPageButton').disabled = false;
						document.getElementById('nextPageButton').disabled = false;
					}else{
						document.getElementById('firstPageButton').disabled = true;		
						document.getElementById('previousPageButton').disabled = true;
						document.getElementById('lastPageButton').disabled = false;
						document.getElementById('nextPageButton').disabled = false;
					}
				}else{
					if(pageNumber < totalSize){
						document.getElementById('lastPageButton').disabled = false;
						document.getElementById('nextPageButton').disabled = false;
						if(pageNumber==1){
							document.getElementById('firstPageButton').disabled = true;
							document.getElementById('previousPageButton').disabled = true;
						}else{
							document.getElementById('firstPageButton').disabled = false;
							document.getElementById('previousPageButton').disabled = false;
						}
					}else{
						document.getElementById('lastPageButton').disabled = true;
						document.getElementById('nextPageButton').disabled = true;
						document.getElementById('firstPageButton').disabled = false;
						document.getElementById('previousPageButton').disabled = false;
					}
				}
				$scope.currentPageNumber = pageNumber;
				$scope.fetchListOfAirports();
			}
			
			$scope.updateOrderByValue=function(value){
				$scope.orderByIdentifier = value;
			}
			
			$scope.resetForm=function(){
				$scope.filterAirportOrg=null;
				$scope.filterAirportDest=null;
				$scope.originSelected = null;
				$scope.destinationSelected = null;
				$scope.fareOutput = null;
				document.getElementById('inputForm').reset();
				document.getElementById('fareQueryResult').innerHTML='';	
			}
			
			$scope.refreshMetrics=function(){
				$scope.populateMetrics();
			}			
			
			$scope.populateMetrics=function(){
				$http({
			        method : "GET",
			        url : "/travel/metrics"
			    }).then(function mySuccess(response) {
			    	$scope.requestMetricsFullJsonData = response.data;
			    	//totalNoOfRequestProcessed
			    	var totalReq = $scope.requestMetricsFullJsonData["totalNoOfRequestProcessed.count"];
			    	if(totalReq){
						$scope.requestMetricsTotalReq = totalReq;			    		
			    	}else{
			    		$scope.requestMetricsTotalReq = 0;
			    	}
			    	var req200 = $scope.requestMetricsFullJsonData["totalNoOf200Request.count"];
			    	if(req200){
			    		$scope.requestMetricsReq200 = req200;		    		
			    	}else{
			    		$scope.requestMetricsReq200 = 0;
			    	}
			    	
			    	var req400 = $scope.requestMetricsFullJsonData["totalNoOf4xxRequestMeter.count"];
			    	if(req400){
			    		$scope.requestMetricsReq400 = req400;		    		
			    	}else{
			    		$scope.requestMetricsReq400 = 0;
			    	}
			    	
			    	var req500 = $scope.requestMetricsFullJsonData["totalNoOf5xxRequestMeter.count"];
			    	if(req500){
			    		$scope.requestMetricsReq500 = req500;		    		
			    	}else{
			    		$scope.requestMetricsReq500 = 0;
			    	}
			    	var avgtime = $scope.requestMetricsFullJsonData["responseTimer.snapshot.mean"];
			    	if(avgtime){
			    		$scope.avgresponsetime = avgtime;		    		
			    	}else{
			    		$scope.avgresponsetime = 0;
			    	}
			    	var mintime = $scope.requestMetricsFullJsonData["responseTimer.snapshot.min"];
			    	if(mintime){
			    		$scope.minresponsetime = mintime;		    		
			    	}else{
			    		$scope.minresponsetime = 0;
			    	}
			    	var maxtime = $scope.requestMetricsFullJsonData["responseTimer.snapshot.max"];
			    	if(maxtime){
			    		$scope.maxresponsetime = maxtime;		    		
			    	}else{
			    		$scope.maxresponsetime = 0;
			    	}
			    	$scope.initBarChart();
			    }, function myError(response) {
			        $scope.apiResponse = response.statusText;
			    });
			}
			
			$scope.initBarChart=function(){
				$scope.myBarChardData = {
					      type : "bar",
					      legend: {},
					      title:{
					        backgroundColor : "transparent",
					        fontColor :"black",
					        text : "API Response Status Chart"
					      },
					      backgroundColor : "white",
					      series : [
					    	 {
					          values : [$scope.requestMetricsTotalReq],
					          text : 'Total requests',
					          backgroundColor : "yellow"
					         
					        },
					        {
					          values : [$scope.requestMetricsReq200],
					          text : 'Total OK response',
					          backgroundColor : "lightgreen"
					         
					        },
					        {
					          values : [$scope.requestMetricsReq400],
					          text : 'Total 4xx response',
					          backgroundColor : "orange"
						    },
						    {
					          values : [$scope.requestMetricsReq500],
					          text : 'Total 5xx response',
					          backgroundColor : "#4DC0CF"
						    }			    
					      ]
					    };
			}
			
			
			$scope.searchFare=function(){
				var fareResultElement = document.getElementById('fareQueryResult');
				fareResultElement.innerHTML = "Querying Result , Please Wait ..... ";
				
				$http({
			        method : "GET",
			        url : "/travel/queryFare/"+$scope.originSelected+"/"+$scope.destinationSelected
			    }).then(function mySuccess(response) {
					var continueSearch = true;
					$scope.fareOutput = response.data;
					var result = "The Fare Selected for the Itenary is : <strong>"+$scope.fareOutput.amount+"</strong> "+$scope.fareOutput.currency;
					fareResultElement.innerHTML=result;
					
			    }, function myError(response) {
			    	fareResultElement.innerHTML = "Error Occured while calculating Fare , Response Status : " +response.statusText;
			    });
			}
		});
		
	
	function showTabChange(obj){
		if(obj!=null){
			var  tabId = obj.panelContainer.context.activeElement.hash;
			var tabTitle = obj.panelContainer.context.activeElement.innerHTML;
			
			if(tabTitle=="View All Airports"){
				document.getElementById('refreshListOfAirports').click();
			}
			if(tabTitle=="Statistics"){
				document.getElementById('refreshMetrics').click();
			}
			if(tabTitle=="Search flights"){
				return false;
			}
		}
	}
